class Scene1 extends Phaser.Scene {
  constructor() {
    super("chargement");
  }

  preload() {
    this.load.image('fond', 'assets/fond.png');
    this.load.image('ligneVie', 'assets/ligneRouge.png');

    this.load.image('player', 'assets/vaisseauJoueurBleu.png');
    this.load.image('vie', 'assets/vie.png');

    this.load.image('alien', 'assets/alienRouge.png');

    this.load.image('tirJoueur', 'assets/tirLaser1.png');

    this.load.spritesheet("explosion", "assets/explosion.png", {
      frameWidth: 16,
      frameHeight: 16
    });

    this.load.spritesheet("laser", "assets/tirLaser1.png", {
      frameWidth: 16,
      frameHeight: 16
    });
  }

  create() {
    this.add.text(200, 300, "Chargement du jeu...");
    this.scene.start("menu");

    this.score = 0;
    this.defaite = false;

    this.anims.create({
      key: "explose",
      frames: this.anims.generateFrameNumbers("explosion"),
      frameRate: 20,
      repeat: 0,
      hideOnComplete: true
    });

    this.anims.create({
      key: "laserAnim",
      frames: this.anims.generateFrameNumbers("laser"),
      frameRate: 20,
      repeat: -1
    });
  }
}