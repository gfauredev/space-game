var parametresJeu = {
  vitesseDepartEnnemis: 20,
  vitesseJoueur: 750,
  vitesseLasersJoueur: 750,
  palierDifficulte: 10,
  variationDifficulte: 10,
  modeAutoSecret: 0,
};

var score = 0;
var meilleurScore = 0;
var perdu = false;

var config = {
  width: 800,
  height: 600,
  scene: [Scene1, Scene2, Scene3],
  pixelArt: true,
  physics: {
    default: "arcade",
    arcade: {
      debug: false,
    },
  },
  parent: "phaser-game",
};

var game = new Phaser.Game(config);

