class Laser extends Phaser.GameObjects.Sprite {
  constructor(scene) {

    super(scene, scene.player.x, scene.player.y - 28, "laser");

    scene.add.existing(this);

    this.play("laserAnim").setScale(2);
    scene.physics.world.enableBody(this);
    this.body.velocity.y = -parametresJeu.vitesseLasersJoueur;

    scene.projectiles.add(this);
  }


  update() {
    if (this.y < 16) {
      this.destroy();
    }
  }
}