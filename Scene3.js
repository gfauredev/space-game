class Scene3 extends Phaser.Scene {
    constructor() {
        super("modeJeu1");
    }

    create() {
        // Creation des images, dans l’ordre
        this.add.image(400, 300, 'fond');
        this.ligneVie = this.physics.add.image(400, 600, 'ligneVie');

        this.player = this.physics.add.image(config.width / 2, 560, 'player').setScale(1.2);

        this.alien1 = this.add.image(config.width / 2 - 150, config.height / 3, "alien");
        this.alien2 = this.add.image(config.width / 2, config.height / 2, "alien");
        this.alien3 = this.add.image(config.width / 2 + 150, config.height / 3, "alien");

        this.vie1 = this.add.image(config.width - 75, 15, 'vie');
        this.vie2 = this.add.image(config.width - 50, 15, 'vie');
        this.vie3 = this.add.image(config.width - 25, 15, 'vie');

        this.scoreAffiche = this.add.text(10, 10, "Score : " + score);

        // Declaration des variables
        this.vie = 3;
        this.jeuPause = true;
        this.vitesseEnnemis = parametresJeu.vitesseDepartEnnemis;

        this.cursors = this.input.keyboard.createCursorKeys();
        this.barreEspace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        this.echap = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ESC);
        this.entree = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ENTER);

        this.aliens = this.physics.add.group();
        this.aliens.add(this.alien1);
        this.aliens.add(this.alien2);
        this.aliens.add(this.alien3);

        this.projectiles = this.add.group();
        this.physics.add.overlap(this.projectiles, this.aliens, this.toucheAlien, null, this);
    }


    deplacementJoueur() {
        if (this.cursors.left.isDown && this.player.x >= 0) {
            this.player.setVelocityX(-parametresJeu.vitesseJoueur);
        } else if (this.cursors.right.isDown && this.player.x <= 800) {
            this.player.setVelocityX(parametresJeu.vitesseJoueur);
        } else {
            this.player.setVelocityX(0);
        }
    }

    deplacementAliens(alien, variation) {
        alien.y += ((this.vitesseEnnemis / 60) + variation);
        if (alien.y > config.height) {
            this.resetAliens(alien);
            this.perteVie();
        }
    }

    resetAliens(alien) {
        alien.y = 0;
        var randomX = Phaser.Math.Between(0, config.width);
        alien.x = randomX;
    }

    perteVie() {
        this.vie--;
        if (this.vie == 2) {
            this.vie1.destroy();
        } else if (this.vie == 1) {
            this.vie2.destroy();
        } else if (this.vie === 0) {
            this.vie3.destroy();
        } else if (this.vie == -1) {
            perdu = true;
            this.scene.start("menu");
        }
    }

    toucheAlien(projectile, alien) {
        var explosion = new Explosion(this, alien.x, alien.y);

        projectile.destroy();
        this.resetAliens(alien);
        score += 1;

        this.scoreAffiche.text = "Score : " + score;

        if (score % parametresJeu.palierDifficulte === 0) {
            this.vitesseEnnemis += parametresJeu.variationDifficulte;
        }

        console.log("Score : ", score, "Vitesse aliens : ", this.vitesseEnnemis);
    }

    tirLaser() {
        var laser = new Laser(this);
    }

    explosion(objet) {
        objet.setTexture("explosion");
        objet.play("explose");
    }


    update() {
        // Boucle ne s’activant que si la variable jeuPause est sur true
        if (this.jeuPause) {
            // Appel de la fonction qui gere le deplacement du joueur
            this.deplacementJoueur();

            // Detection de la barre d’epace pour tirer
            if (this.barreEspace.isDown && parametresJeu.modeAutoSecret == 2020) {
                this.tirLaser();
            } else if (Phaser.Input.Keyboard.JustDown(this.barreEspace)) {
                this.tirLaser();
            }

            // Deplacement des aliens
            this.deplacementAliens(this.alien1, 1);
            this.deplacementAliens(this.alien2, 2);
            this.deplacementAliens(this.alien3, 3);
        }

        // Ajout des projectiles au meme groupe puis actualisation pour le supprimer si il depasse un certain y
        for (var i = 0; i < this.projectiles.getChildren().length; i++) {
            var laser = this.projectiles.getChildren()[i];
            laser.update();
        }

        // Detection de la touche escape pour stopper le jeu
        if (Phaser.Input.Keyboard.JustDown(this.echap)) {
            this.scene.start("menu");
        }

        // Detection de la touche entree pour mettre en pause
        if (Phaser.Input.Keyboard.JustDown(this.entree)) {
            this.jeuPause = !this.jeuPause;
        }
    }
}