class Scene2 extends Phaser.Scene {
  constructor() {
    super("menu");
  }

  create() {
    this.add.image(400, 300, 'fond');
    this.ligneVie = this.physics.add.image(400, 600, 'ligneVie');

    this.add.text(400, 150, "Menu", {
      font: "60px Arial",
      fill: "#6a8aaa"
    }).setOrigin(0.5, 0.5);

    if (perdu) {
      this.add.text(400, 250, "Perdu !", {
        font: "50px Arial",
        fill: "#ff5555"
      }).setOrigin(0.5, 0.5);
    }

    this.add.text(400, 350, "Score précédant : " + score).setOrigin(0.5, 0.5);

    if (score > meilleurScore) {
      meilleurScore = score;
    }

    this.add.text(400, 375, "Meilleur score : " + meilleurScore).setOrigin(0.5, 0.5);
    this.add.text(400, 450, "Pressez espace pour jouer").setOrigin(0.5, 0.5);

    this.cursors = this.input.keyboard.createCursorKeys();
    this.barreEspace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
    this.echap = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ESC);
    this.entree = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ENTER);

    this.player = this.physics.add.image(config.width / 2, 560, 'player').setScale(1.2);
  }


  choixParametres() {
    console.log(parametresJeu);

    var choix = prompt("- Vitesse de depart des ennemis, Vitesse de joueur, Vitesse des lasers, Paliers de difficulte, Variation de la difficulte \n- Les valeurs sont en px par seconde sauf palier difficulte qui signifie les paliers de score a passer pour augmenter la difficulte \n- Veuillez entrer TOUTES les valeurs séparées par des virgules ou faire annuler \n- Vous pouvez connaitre les valeurs actuelles dans la console (ctrl+maj+i)");

    if (choix) {
      choix = choix.split(",").map(Number);
      parametresJeu = {
        vitesseDepartEnnemis: choix[0],
        vitesseJoueur: choix[1],
        vitesseLasersJoueur: choix[2],
        palierDifficulte: choix[3],
        variationDifficulte: choix[4],
        modeAutoSecret: choix[5],
      };
    }

    console.log(parametresJeu);
  }


  update() {
    // Detection des fleches directionnelles pour deplacer le vaisseau
    if (this.cursors.left.isDown && this.player.x >= 0) {
      this.player.setVelocityX(-parametresJeu.vitesseJoueur);
    } else if (this.cursors.right.isDown && this.player.x <= 800) {
      this.player.setVelocityX(parametresJeu.vitesseJoueur);
    } else {
      this.player.setVelocityX(0);
    }

    // Detection des touches espace et echap pour lancer le jeu
    if (Phaser.Input.Keyboard.JustDown(this.barreEspace) || Phaser.Input.Keyboard.JustDown(this.echap)) {
      this.scene.start("modeJeu1");
      score = 0;
      perdu = false;
    }

    // Detection de la touche entree pour mettre en pause
    if (Phaser.Input.Keyboard.JustDown(this.entree)) {
      this.choixParametres();
    }
  }
}